<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // usamos faker para crear contenido en la tabla
        $faker = Faker\Factory::create();
        // limitamos el numero de datos creados con faker
        $limit = 20;
        // creamos los datos
        for ($i = 0; $i < $limit; $i++) {
            Product::create([
                'product_name' => $faker->name,
                'description' => $faker->text(250), 
                'total' => $faker->numberBetween(1,20)
            ]);
        }
    }
}
