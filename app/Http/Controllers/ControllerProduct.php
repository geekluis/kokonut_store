<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
// usamos el modelo product
use App\Product;

class ControllerProduct extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // traemos todos los datos de la tabla
        $data = Product::all();
        if (count($data) > 0) {
            $response['message'] = "success";
            $response['data']= $data;
            return response()->json(['data' => $response], 200) ;
        } else {
            $res['message'] = "empty";
            return response($res);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
        * validamos los datos que deben llegar en el request 
        * y asiganos mensajes de error por valores requeridos 
        */
        $validator = Validator::make($request->all(), [
            'product_name' => 'required|string|max:100',
            'description' => 'required|string|max:250',
            'total' => 'required|integer'
        ],
        [
            'product_name.required' => 'El nombre es requerido',
            'description.required' => 'Descripción requerida',
            'total.required' => 'Total requerido'
        ]);
        // si faltan dados respondemos con error
        if ($validator->fails()) { 
            $response['message'] = "error";
            $response['errors'] = $validator->customMessages;
            return response()->json(['data' => $response], 400) ;
        } else {
            // guadamos el request en una variable
            $inputs = $request->all();
            // creamos el nuevo producto con eloquent
            $product = new Product;
            $product->product_name = $inputs['product_name'];
            $product->description = $inputs['description'];
            $product->total = $inputs['total'];
            $product->save();
            // respondemos en formato json el producto agregado
            $response['message'] = "success";
            $response['data']= $product;
            return response()->json(['data' => $response], 201) ;
        }
    }

}
