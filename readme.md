## Kokonut Store

Sistema trabajado en `laravel 5.4.*` este sistema cuenta con 2 endpoints:
- Get {{url}}/api/product
- Post {{url}}/api/product

### GET 

Es el endpoint para poder consultar los productos dados de alta en la tabla, solo es una consulta bajo una peticion get.

### POST

Es el enpoint para agregar productos en la tabla products.

### POSTMAN

En este caso tenemos una documentación creada en postman el cual se peude ver en esta url [aquí](https://documenter.getpostman.com/view/3281631/SVfJVBmb?version=latest#4e669569-7d01-4cac-a606-b3ba7bf5ba1c)

### ARCHIVOS

En caso de requerir descargar el proyecto fuera de gitlab se puede acceder por esta url, en esta url esta el archivo zip del proyecto y se agrego la base mysql con la que se trabajaron las pruebas.
[Link](https://drive.google.com/drive/folders/1S3vXngvGL7PQgDgdslkfAW5HeavWieLA?usp=sharing)


### Respuestas de Examen

Coloco las respuestas del examen aquí ya que trabajo con LibreOfficce y pude afectar la vista del archivo:

Responde la manera mas clara posible las siguientes preguntas:

    1. ¿Qué es mejor? utilizar un framework o solo utilizar el lenguaje de manera “vanilla” el lenguaje en cuestión. 
        R= Es mejor usar un framework, los frameworks ayudan en agilizar el desarrollo de una aplicación, ademas de contar con mejoras y ayuda en la seguridad de una aplicación.
        
    2. ¿Cuál considerarías la principal diferencia entre PostgreSQL y MySQL? 
        R= MySql es más comercial y hecho para sistemas un tanto simples, mientras que PostgreSQL es para sistemas más robustos. 

    3. ¿Para ti cual sería la principal diferencia entre usar nginx y apache? 
        R= Desconozco un poco el tema pero me parece que la popularidad de cada uno es una gran diferencia Apache es más utilizado que Nginx.